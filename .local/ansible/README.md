```[bash]
ansible-playbook --ask-become-pass bootstrap-system.yml
```

```[bash]
ansible-playbook --vault-password-file .ansible_vault_password --ask-become-pass bootstrap-system.yml
```
